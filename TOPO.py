import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.transforms import Bbox
import networkx as nx

import seaborn as sns

import collections

from time import time

#import bs4 as bs  
import urllib.request  
import re  
import math
import os
import cv2

def kaze_descriptor(image, imageMeanVal, tileName, firstFile, indexTotal):
    try:
        
        kaze = cv2.KAZE_create()
        column_list = ['id_total', 'id_tile', 'tile_name', 'x_pix', 'y_pix']
        
        kps = kaze.detect(image)
        invalid_kps = [] # List used to store any keypoint that lies near the border (or in the border) of the image
        keypointDataframe = [] # List used to store the data regarding each keypoint in the file
        
        for keypoint in kps:
            x_key = int(keypoint.pt[0])
            y_key = int(keypoint.pt[1])
            
            x_min, x_max = x_key - 10, x_key + 10
            y_min, y_max = y_key - 10, y_key + 10
            
            if x_key < 10:
                x_min = 0
            if x_key > 1151:
                x_max = 1161
            if y_key < 10:
                y_min = 0
            if y_key > 1560:
                y_max = 1570
            
            
            window_key = image[y_min : y_max, x_min : x_max].flatten()
                
            if any(i in window_key for i in imageMeanVal):
                #print("Invalid")
                invalid_kps.append(keypoint)
            
        
        valid_kps = [keypoint for keypoint in kps if keypoint not in invalid_kps]
        
        # Create the Pandas Dataframe and save it in a csv file
        index_keypoint = 0
        for keypoint in valid_kps:
            keypointDataframe.append({'id_total': indexTotal, 'id_tile': index_keypoint, 'tile_name': tileName, 
                                      'x_pix': keypoint.pt[1], 'y_pix': keypoint.pt[0]})
            index_keypoint += 1
            indexTotal += 1
        
        
        keypointDf = pd.DataFrame(keypointDataframe)
        with open('keypointDataframe.csv', 'a') as csvFile:
            if firstFile:
                keypointDf.to_csv(csvFile, header=True, columns=column_list)
            else:
                keypointDf.to_csv(csvFile, header=False, columns=column_list)
                
        
        print("Key: ", len(list(valid_kps)), len(list(kps)))    
        valid_kps, valid_dscs = kaze.compute(image, valid_kps)
            
    except cv2.error as e:
        print('Error: ', e)
        return None
    
    return valid_kps, valid_dscs, indexTotal

def sift_descriptor(image):
    try:
        imageGray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        sift = cv2.xfeatures2d.SIFT_create()
        (kps, dscs) = sift.detectAndCompute(imageGray, None)
        
    except cv2.error as e:
        print('Error: ', e)
        return None
    
    return kps, dscs

def surf_descriptor(image):
    try:
        imageGray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        surf = cv2.xfeatures2d.SURF_create()
        (kps, dscs) = surf.detectAndCompute(imageGray, None)
        
    except cv2.error as e:
        print('Error: ', e)
        return None
    
    return kps, dscs

def match_computation(kpsList1, kpsList2, tileName1, tileName2):
    
    descList1 = np.array([kps['desc'] for kps in kpsList1])
    descList2 = np.array([kps['desc'] for kps in kpsList2])
    
    print("LEN: ", len(kpsList1), len(kpsList2))
    
    bf = cv2.BFMatcher()
    matches = bf.knnMatch(descList1, descList2, k=1)
    
    matchList = [] # List used to store the data related to each match
    if os.path.isfile('matchesDataframe.csv'):
        matchDF = pd.read_csv('matchesDataframe.csv')
        indexMatch = matchDF.shape[0]
    else:
        indexMatch = 0
    
    #matchDF = pd.read_csv()
    indexRange = np.arange(indexMatch, indexMatch + len(matches))
    
    distanceList = [m[0].distance for m in matches]
    
    kps1List = [kpsList1[m[0].queryIdx] for m in matches]
    kps2List = [kpsList2[m[0].trainIdx] for m in matches]
    
    matchList = [{'id_total': indexMatch, 'tile_nameA': tileName1, 'x_pixA': kps1['x'], 'y_pixA': kps1['y'],
                          'tile_nameB': tileName2, 'x_pixB': kps2['x'], 'y_pixB': kps2['y'], 'distance': distance}
                for indexMatch, kps1, kps2, distance in zip(indexRange, kps1List, kps2List, distanceList)]
    
    '''
    for m in matches:
        
        matchTime = time()
        
        desc1 = descList1[m[0].queryIdx]
        desc2 = descList2[m[0].trainIdx]
        distance = m[0].distance
        
        kps1 = [kps for kps in kpsList1 if (np.array(kps['desc']) == desc1).all()]
        kps2 = [kps for kps in kpsList2 if (np.array(kps['desc']) == desc2).all()]
        
        matchList.append({'id_total': indexMatch, 'tile_nameA': tileName1, 'x_pixA': kps1[0]['x'], 'y_pixA': kps1[0]['y'],
                          'tile_nameB': tileName2, 'x_pixB': kps2[0]['x'], 'y_pixB': kps2[0]['y'], 'distance': distance})
        
        #print("MatchTime: ", time() - matchTime)
        if np.mod(indexMatch, 1000) == 0:
            print("Count: ", indexMatch)
            
        indexMatch += 1
    '''
        
    matchesDF = pd.DataFrame(matchList)
    column_list = ['id_total', 'tile_nameA', 'x_pixA', 'y_pixA', 'tile_nameB', 'x_pixB', 'y_pixB', 'distance']
    with open('matchesDataframe.csv', 'a') as csvFile:
        matchesDF.to_csv(csvFile, header=True, columns=column_list)

    return matches

def repeatability_computation(image1, kps1, desc1, image2, kps2, desc2):
    
    bf = cv2.BFMatcher()
    matches = bf.knnMatch(desc1, desc2, k=2)
    
    good = []
    for m, n in matches:
        if m.distance < 0.95 * n.distance:
            good.append([m])
            
    print("Repeatability: ", len(good) / np.min([len(desc1), len(desc2)]))
    
def draw_keypoints(img, kps):
    
    canvas = cv2.cvtColor(img,cv2.COLOR_RGB2BGR)
    
    for keypoint in kps:
        x_key = int(keypoint.pt[0])
        y_key = int(keypoint.pt[1])
        magnitude_key = int(keypoint.size)
        orientation_key = keypoint.angle * math.pi / 180

        cv2.circle(canvas, (x_key, y_key), magnitude_key, (255, 0, 0), 3)

        x_end = int(x_key + magnitude_key * math.cos(orientation_key))
        y_end = int(y_key + magnitude_key * math.sin(orientation_key))

        cv2.line(canvas, (x_key, y_key), (x_end, y_end), (255, 0, 0), 3)
    
    return canvas